#!/bin/sh

helm repo add fluxcd https://charts.fluxcd.io
helm repo add eks https://aws.github.io/eks-charts

kubectl apply -f https://raw.githubusercontent.com/fluxcd/helm-operator/1.2.0/deploy/crds.yaml
kubectl create namespace flux

# config values : https://github.com/fluxcd/flux/blob/master/chart/flux/values.yaml
helm upgrade -i flux fluxcd/flux \
  --set git.url=git@bitbucket.org:adam-kaplan-axian/example.git \
  --set git.path=devops/flux \
  --namespace flux

helm upgrade -i helm-operator fluxcd/helm-operator \
  --set git.ssh.secretName=flux-git-deploy \
  --set helm.versions=v3 \
  --namespace flux

# get the git ssh public key from flux installation
# key needs to be added to git remote repository
# fluxctl identity --k8s-fwd-ns flux

# trigger flux sync
# fluxctl sync --k8s-fwd-ns flux