
resource "aws_kms_key" "vpc_flow_log" {
  description = "Key for [${local.name_prefix}-vpc] flow log encryption."
  deletion_window_in_days = 7
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "2.48.0"

  name = "${local.name_prefix}-vpc"
  cidr = local.vpc_cidr
  azs = local.availability_zones
  private_subnets = local.vpc_private_subnet_cidrs
  public_subnets = local.vpc_public_subnet_cidrs
  enable_nat_gateway = true
  single_nat_gateway = true
  enable_dns_hostnames = true
  enable_dns_support = true

  // Required Configuration
  database_subnet_assign_ipv6_address_on_creation = false
  default_security_group_egress = [
    {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_block = "0.0.0.0/0"
    }
  ]
  default_security_group_ingress = [
    {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_block = "0.0.0.0/0"
    },
    {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_block = local.vpc_cidr
    }
  ]
  elasticache_subnet_assign_ipv6_address_on_creation = false
  enable_classiclink = false
  enable_classiclink_dns_support = false
  flow_log_cloudwatch_log_group_kms_key_id = aws_kms_key.vpc_flow_log.id
  flow_log_cloudwatch_log_group_retention_in_days = 7
  flow_log_log_format = "<version> <account-id> <interface-id> <srcaddr> <dstaddr> <srcport> <dstport> <protocol> <packets> <bytes> <start> <end> <action> <log-status>"
  intra_subnet_assign_ipv6_address_on_creation = false
  private_subnet_assign_ipv6_address_on_creation = false
  public_subnet_assign_ipv6_address_on_creation = false
  redshift_subnet_assign_ipv6_address_on_creation = false
  vpn_gateway_az = local.availability_zones[0]

  // Custom Tagging
  tags = merge(local.default_tags, {
    "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
  })

  private_subnet_tags = merge(local.default_tags, {
    "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb" = 1
  })

  public_subnet_tags = merge(local.default_tags, {
    "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
    "kubernetes.io/role/elb" = 1
  })
}