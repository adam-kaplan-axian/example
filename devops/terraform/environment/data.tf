data "terraform_remote_state" "globals" {
  backend = "s3"

  config = {
    bucket = "act-on-terraform-backend"
    key    = "global/terraform.tfstate"
  }
}

data "aws_region" "current" {}

data "aws_availability_zones" "available" {
  state = "available"
  filter {
    name = "region-name"
    values = [data.aws_region.current.name]
  }
}

data "aws_eks_cluster" "default" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "default" {
  name = module.eks.cluster_id
}