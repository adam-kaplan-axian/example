locals {

  name_prefix = "${data.terraform_remote_state.globals.outputs.default_name_prefix}-${terraform.workspace}"

  default_tags = merge(
    data.terraform_remote_state.globals.outputs.default_tags,
    {
      env = terraform.workspace
    }
  )

  eks_cluster_name = "${local.name_prefix}-eks-2"

  availability_zones = slice(data.aws_availability_zones.available.names, 0, 3)

  vpc_cidr = data.terraform_remote_state.globals.outputs.workspace_vpc_cidrs[terraform.workspace]
  vpc_subnet_count = length(local.availability_zones) * 2
  vpc_subnet_cidrs = cidrsubnets(local.vpc_cidr, [for index in range(local.vpc_subnet_count): ceil(log(local.vpc_subnet_count, 2))]...)
  vpc_private_subnet_cidrs = slice(local.vpc_subnet_cidrs, 0, local.vpc_subnet_count / 2)
  vpc_public_subnet_cidrs = slice(local.vpc_subnet_cidrs, local.vpc_subnet_count / 2, local.vpc_subnet_count)

}