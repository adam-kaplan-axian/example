module "eks" {
  source = "terraform-aws-modules/eks/aws"
  version = "12.2.0"

  // required inputs
  cluster_name = local.eks_cluster_name
  subnets = concat(module.vpc.private_subnets, module.vpc.public_subnets)
  vpc_id = module.vpc.vpc_id

  // tagging
  tags = local.default_tags

  // optional inputs
  cluster_version = "1.17"
  attach_worker_cni_policy = true
  manage_worker_iam_resources = true
  worker_create_cluster_primary_security_group_rules = true
  worker_create_initial_lifecycle_hooks = true
  worker_create_security_group = true
  write_kubeconfig = false

  // config options : https://github.com/terraform-aws-modules/terraform-aws-eks/blob/master/local.tf#L36
  worker_groups = [
    {
      name                          = "kube-system"
      instance_type                 = "t2.small"
      asg_desired_capacity          = 2
    }
  ]
}

resource "aws_eks_fargate_profile" "eks_application" {
  cluster_name = local.eks_cluster_name
  fargate_profile_name = "${local.name_prefix}-eks-application"
  pod_execution_role_arn = aws_iam_role.fargate.arn
  subnet_ids = module.vpc.private_subnets

  selector {
    namespace = "default"
  }

  selector {
    namespace = "flux"
  }

  tags = merge(local.default_tags, {
    Name = "${local.name_prefix}-eks-application"
  })

  depends_on = [
    module.vpc,
    module.eks
  ]
}

resource "aws_iam_role" "fargate" {
  name = "${local.name_prefix}-eks-fargate-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = [
            "eks.amazonaws.com",
            "eks-fargate-pods.amazonaws.com"
          ]
        }
      }]
  })

  tags = merge(local.default_tags, {
    Name = "${local.name_prefix}-eks-fargate-role"
  })
}

resource "aws_iam_role_policy_attachment" "fargate-AmazonEKSFargatePodExecutionRolePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSFargatePodExecutionRolePolicy"
  role = aws_iam_role.fargate.name
}
