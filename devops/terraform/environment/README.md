# Environment Terraform #
Terraform that creates AWS resources for a target environment.

## Prerequisites ##

1. `terraform init`
2. `terraform workspace new dev`

## Post Deployment ##
Perform the following tasks after successful terraform apply.

1. Run `scripts/flux-init.sh`
    * Requires:
        * kubectl
        * helm

## kubectl ##
[Install](https://kubernetes.io/docs/tasks/tools/install-kubectl/) kubectl for 
your system.

Setup configuration for the kubectl command. [aws-cli](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/eks/update-kubeconfig.html)
```
aws eks update-kubeconfig --name act-on-dev-eks
```

## Links ##

* [VPC For Fargate](https://docs.aws.amazon.com/eks/latest/userguide/create-public-private-vpc.html)