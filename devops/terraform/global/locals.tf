locals {
  name_prefix = "act-on"

  default_tags = {
    client = local.name_prefix
    purpose = "learning-and-development"
  }

  ecr_all_repos = {
    java-api = aws_ecr_repository.java-api.repository_url
    java-api-helm = aws_ecr_repository.java-api-helm.repository_url
    node-api = aws_ecr_repository.node-api.repository_url
    node-api-helm = aws_ecr_repository.node-api-helm.repository_url
  }
}