resource "local_file" "ecr_java_properties" {
  content     = join( "\n", [for name,url in local.ecr_all_repos: "ecr.${name}=${url}"])
  filename = "${path.cwd}/out/ecr-java.properties"
}
resource "local_file" "ecr_node_json" {
  content     = jsonencode(local.ecr_all_repos)
  filename = "${path.cwd}/out/ecr-node.json"
}