# Global Terraform #
Terraform that creates AWS resources that need to be available
globally to all other terraform scripts.

## Prerequisites ##

1. `terraform init`

Always use default workspace, since all values will need to 
be configured across all possible workspaces.

## ECR ##
ECR needs to be globally available, so that images only need to be pushed 
to a single docker repository.

Terraform commands to obtain access id and secret.
```
terraform output ecr_user_key
terraform output ecr_user_secret
```