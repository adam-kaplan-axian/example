terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.4.0"
    }
  }
  backend "s3" {
    bucket = "act-on-terraform-backend"
    key    = "global/terraform.tfstate"
  }
}

provider "aws" {
  profile = "terraform-user"
}