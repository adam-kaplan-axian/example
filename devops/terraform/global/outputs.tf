

output "default_tags" {
  description = "Standard tags to apply to all taggable resources."
  value = local.default_tags
}

output "default_name_prefix" {
  description = "Prefix to use for resource names."
  value = local.name_prefix
}

output "workspace_vpc_cidrs" {
  description = "Mapping of workspace names to CIDR to use for VPC configuration."
  value = {
    dev = "10.3.0.0/16"
  }
}

output "docker_repositories" {
  description = "Repositories to store docker images."
  value = local.ecr_all_repos
}

output "ecr_user_key" {
  description = "Access Key ID for the ecr_push user."
  value = aws_iam_access_key.ecr_push.id
  sensitive = true
}

output "ecr_user_secret" {
  description = "Secret Access Key for the ecr_push user."
  value = aws_iam_access_key.ecr_push.secret
  sensitive = true
}