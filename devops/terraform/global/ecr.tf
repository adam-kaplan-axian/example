
resource "aws_ecr_repository" "java-api" {
  name                 = "${local.name_prefix}/java-api"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags = merge(local.default_tags, {
    Name = "java-api"
  })
}

resource "aws_ecr_repository" "java-api-helm" {
  name                 = "${local.name_prefix}/java-api-helm"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags = merge(local.default_tags, {
    Name = "java-api-helm"
  })
}

resource "aws_ecr_repository" "node-api" {
  name                 = "${local.name_prefix}/node-api"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags = merge(local.default_tags, {
    Name = "node-api"
  })
}

resource "aws_ecr_repository" "node-api-helm" {
  name                 = "${local.name_prefix}/node-api-helm"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags = merge(local.default_tags, {
    Name = "node-api-helm"
  })
}

resource "aws_iam_user" "ecr_push" {
  name = "${local.name_prefix}-ecr-push-user"

  tags = merge(local.default_tags, {
    Name = "${local.name_prefix}-ecr-push-user"
  })
}

resource "aws_iam_user_policy_attachment" "ecr_push" {
  user       = aws_iam_user.ecr_push.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"
}

resource "aws_iam_access_key" "ecr_push" {
  user = aws_iam_user.ecr_push.name
}