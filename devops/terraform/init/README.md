# Init Terraform #
Steps needed to start from scratch.

## Prerequisites ##

1. AWS Account to Execute terraform.
2. Create s3 bucket to store terraform state.

        echo "aws s3api create-bucket --bucket act-on-terraform-backend --create-bucket-configuration LocationConstraint=$(aws configure get region)" | bash
        aws s3api put-bucket-tagging --bucket act-on-terraform-backend --tagging 'TagSet=[{Key=client,Value=act-on},{Key=purpose,Value=learning-and-development}]'
        
