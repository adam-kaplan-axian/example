package org.example.javaapi.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
public class Hello {
    private UUID id;
    private String message;
}
