package org.example.javaapi.service;

import org.example.javaapi.model.Hello;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class HelloService {

    public Hello random() {
        Hello hello = new Hello();
        hello.setId(UUID.randomUUID());
        hello.setMessage("Hello!");
        return hello;
    }
}
