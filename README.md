# Kube Sample #
Project to test kubernetes.

## Prerequisites ##

1. java 11
2. node 10
3. docker 19
4. [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) 1.16.6
5. [helm](https://helm.sh/docs/intro/install/) 3.3.1
6. [fluxctl](https://docs.fluxcd.io/en/latest/references/fluxctl/) 1.20.2

## Local Development ##

1. [Install Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
2. `minikube start --driver=docker`
3. `minikube config set driver docker`
4. `eval $(minikube docker-env)` - Uses minikube's docker. [Ref](https://minikube.sigs.k8s.io/docs/handbook/pushing/#3-pushing-directly-to-in-cluster-cri-o-podman-env)

For Each Service

1. Execute dockerize commands.
2. Execute helmDeploy commands.
3. `minikube service --url $SERVICE_NAME` - this command may need to remain in an active process

## Deployment ##
1. Apply current state of [terraform](/devops/README.md)
    * Artifacts:
        * `/devops/terraform/global/out`
2. For Each Project.
    1. Build
    2. Test
    3. Dockerize
    4. pushToECR
    5. pushHelmToECR
3. TODO : Rev Version
4. 

## Links ##

* [Repository](https://bitbucket.org/adam-kaplan-axian/example/src/master/)