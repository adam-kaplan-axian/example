const express = require('express')
const uuid = require('uuid')
const app = express()
const port = 8080

app.get('/health', (req, res) => {
  res.json({status: 'UP'})
})

app.get('/hello', (req, res) => {
  res.json({
   id: uuid.v4(),
   message: 'Hello!'
  })
})

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
})