const child_process = require('child_process');
const path = require('path');
const fs = require('fs');
const name = process.env.npm_package_name;
const version = process.env.npm_package_name

async function spawn(command, args, options) {
    options = options || {};
    return new Promise((resolve, reject) => {
        const proc = child_process.spawn(command, args);

        proc.stdout.on('data', options.stdout || function (data) { console.log(data.toString()); });
        proc.stderr.on('data', (data) => console.error(data.toString()));

        proc.on('error', console.error);
        proc.on('exit', (code, signal) => {
            const result = { code, signal };
            if (code === 0) {
                resolve(result);
            } else {
                console.error(`failure: ${command} ${args.join(' ')}`)
                reject(result);
            }
        });
    });
}

async function readJSON() {
    const jsonFile = path.normalize(`${process.cwd()}/../devops/terraform/global/out/ecr-node.json`);
    return new Promise((resolve, reject) => {
        fs.readFile(jsonFile, 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            resolve(data.toString());
        })
    })
    .then(JSON.parse);
}

(async function start() {
    let ecrPassword

    function getPasswordStout(data) {
        ecrPassword = data.toString();
    }

    const ecr = await readJSON().then(json => json[name]);
    const ecrDomain = ecr.split('/')[0];

    await spawn('aws', ['ecr', 'get-login-password', '--profile', 'ecr-push'], {stdout:getPasswordStout})
        .then(() => {
            return spawn('docker', ['login', '--username', 'AWS', '--password', ecrPassword, ecrDomain]);
        })
        .then(() => {
            return spawn('docker', ['tag', `${name}:latest`, `${ecr}:latest`]);
        })
        .then(() => {
            return spawn('docker', ['tag', `${name}:${version}`, `${ecr}:${version}`]);
        })
        .then(() => {
            return spawn('docker', ['push', ecr]);
        })
        .catch((error) => {
            console.error('Stopping', error);
        });
})();